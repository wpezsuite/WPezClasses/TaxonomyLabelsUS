<?php
/**
 * Class TaxonomyLabelsUS
 *
 * @package WPezSuite\WPezClasses\TaxonomyLabelsUS
 */

namespace WPezSuite\WPezClasses\TaxonomyLabelsUS;

// No WP? No good.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * "Automates" the creation of the labels array for a WordPress taxonomy in US English.
 */
class ClassTaxonomyLabelsUS {

	/**
	 * Taxonomy name singular.
	 *
	 * @var string
	 */
	protected $str_name_singular;

	/**
	 * Taxonomy name plural.
	 *
	 * @var string
	 */
	protected $str_name_plural;

	/**
	 * Setters return nothing but the result of a given set is available via this property and getSetReturn().
	 *
	 * @var mixed
	 */
	protected $mix_set_return;

	/**
	 * Class constructor.
	 */
	public function __construct() {

		$this->setPropertyDefaults();
	}

	/**
	 * Set the class property defaults.
	 */
	protected function setPropertyDefaults() {

		$this->str_name_singular = false;
		$this->str_name_plural   = false;
		$this->mix_set_return    = null;
	}

	/**
	 * Returns the current value of the protected property $mix_set_return.
	 *
	 * @return mixed
	 */
	public function getSetReturn() {

		return $this->mix_set_return;
	}

	/**
	 * Set the taxonomy name singular.
	 *
	 * @param string $str The taxonomy name singular.
	 */
	public function setNameSingular( string $str = '' ) {

		$this->mix_set_return = $this->setNameMaster( 'str_name_singular', $str );
	}

	/**
	 * Set the taxonomy name plural.
	 *
	 * @param string $str The taxonomy name plural.
	 */
	public function setNamePlural( string $str = '' ) {

		$this->mix_set_return = $this->setNameMaster( 'str_name_plural', $str );
	}

	/**
	 * Master setter for the str_name_singular and str_name_plural properties.
	 *
	 * @param string $str_prop The property to set.
	 * @param string $str      The value to set.
	 *
	 * @return bool
	 */
	protected function setNameMaster( string $str_prop, string $str ) {

		$str = esc_attr( $str );
		if ( ! empty( $str ) ) {

			$this->$str_prop = $str;
			return true;
		}

		return false;
	}

	/**
	 * Sets the properties: str_name_singular and str_name_plural.
	 *
	 * @param string $str_singular The taxonomy name singular.
	 * @param string $str_plural   The taxonomy name plural.
	 *
	 * @return void
	 */
	public function setNames( string $str_singular = '', string $str_plural = '' ) {

		$this->mix_set_return = false;
		if ( ! empty( $str_singular ) ) {

			if ( empty( $str_plural ) ) {
				$str_plural = $this->makePlural( $str_singular );
			}
			$this->setNameSingular( $str_singular );
			$this->setNamePlural( $str_plural );

			$this->mix_set_return = true;
		}
	}

	/**
	 * Returns a string with an 's' appended.
	 *
	 * @param string $str The string to pluralize.
	 *
	 * @return string
	 */
	protected function makePlural( string $str = '' ) {

		return $str . 's';
	}

	/**
	 * Returns the labels array.
	 *
	 * @return array
	 */
	public function getLabels() {

		// Once the singular_name and plural_name are set, everything else is taken care of.
		if ( false === $this->str_name_singular || false === $this->str_name_plural ) {
			return array();
		}

		// Ref: https://developer.wordpress.org/reference/functions/get_taxonomy_labels/ .
		$arr_labels = array(
			// 'name' - general name for the taxonomy, usually plural. The same as and overridden by $tax->label. Default is _x( 'Post Tags', 'taxonomy general name' ) or _x( 'Categories', 'taxonomy general name' ).
			'name'                        => $this->str_name_plural,

			// 'menu_name' - the menu name text. This string is the name to give menu items. If not set, defaults to value of name label.
			'menu_name'                   => $this->str_name_plural,

			// 'singular_name' - name for one object of this taxonomy. Default is _x( 'Post Tag', 'taxonomy singular name' ) or _x( 'Category', 'taxonomy singular name' ).
			'singular_name'               => $this->str_name_singular,

			// 'search_items' - the search items text. Default is __( 'Search Tags' ) or __( 'Search Categories' )
			'search_items'                => 'Search ' . $this->str_name_plural,

			// 'popular_items' - the popular items text. This string is not used on hierarchical taxonomies. Default is __( 'Popular Tags' ) or null
			'popular_items'               => 'Popular ' . $this->str_name_plural,

			// 'all_items' - the all items text. Default is __( 'All Tags' ) or __( 'All Categories' )
			'all_items'                   => 'All ' . $this->str_name_plural,

			// 'parent_item' - the parent item text. This string is not used on non-hierarchical taxonomies such as post tags. Default is null or __( 'Parent Category' )
			'parent_item'                 => 'Parent ' . $this->str_name_singular,

			// 'parent_item_colon' - The same as parent_item, but with colon : in the end null, __( 'Parent Category:' )
			'parent_item_colon'           => 'Parent ' . $this->str_name_singular . ':',

			// 'name_field_description - Description for the Name field on Edit Tags screen.
			'name_field_description'      => 'The name is how it appears on your site',

			// Description for the Slug field on Edit Tags screen.
			'slug_field_description'      => 'The "slug" is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens',

			// Description for the parent field on Edit Tags screen.
			'parent_field_description '   => 'Assign a parent term to create a hierarchy. The term Jazz, for example, would be the parent of Bebop and Big Band',

			// Description for the Description field on Edit Tags screen. default 'The description is not prominent by default; however, some themes may show it'.
			'desc_field_description'      => 'The description is not prominent by default; however, some themes may show it',

			// 'edit_item' - the edit item text. Default is __( 'Edit Tag' ) or __( 'Edit Category' )
			'edit_item'                   => 'Edit ' . $this->str_name_singular,

			// 'view_item' - the view item text, Default is __( 'View Tag' ) or __( 'View Category' )
			'view_item'                   => 'View ' . $this->str_name_singular,

			// 'update_item' - the update item text. Default is __( 'Update Tag' ) or __( 'Update Category' ).
			'update_item'                 => 'Update ' . $this->str_name_singular,

			// add_new_item' - the add new item text. Default is __( 'Add New Tag' ) or __( 'Add New Category' ).
			'add_new_item'                => 'Add New ' . $this->str_name_singular,

			// 'new_item_name' - the new item name text. Default is __( 'New Tag Name' ) or __( 'New Category Name' )
			'new_item_name'               => 'New ' . $this->str_name_singular . ' Name',

			// This label is only used for non - hierarchical taxonomies.
			'separate_items_with_commas ' => 'Separate ' . $this->str_name_plural . ' with commas',

			// This label is only used for non - hierarchical taxonomies . default 'Add or remove tags', used in the meta box when JavaScript is disabled.
			'add_or_remove_items'         => 'Add or remove ' . $this->str_name_plural,

			// This label is only used on non - hierarchical taxonomies . default 'Choose from the most used tags', used in the meta box.
			'choose_from_most_used'       => 'Choose from the most used ' . $this->str_name_plural,

			// default 'No tags found' / 'No categories found', used in the meta box and taxonomy list table.
			'not_found'                   => 'Sorry. Search found no ' . $this->str_name_plural,

			// default 'No tags' / 'No categories', used in the posts and media list tables.
			'no_terms'                    => 'No ' . $this->str_name_singular . ' terms',

			// This label is only used for hierarchical taxonomies . default 'Filter by category', used in the posts list table.
			'filter_by_item'              => 'Filter by ' . $this->str_name_singular,

			// Label for the table pagination hidden heading.
			'items_list_navigation'       => $this->str_name_plural . ' list navigation',

			// Label for the table hidden heading.
			'items_list'                  => $this->str_name_plural . ' list',

			// Title for the Most Used tab. Default 'Most Used'.
			'most_used'                   => 'Most Used ' . $this->str_name_plural,

			// The text displayed after a term has been updated for a link back to main index. Default is __( '← Back to tags' ) or __( '← Back to categories' ).
			'back_to_items'               => 'Back to ' . $this->str_name_plural,

			// Used in the block editor . Title for a navigation link block variation .default 'Tag Link' / 'Category Link'.
			'item_link'                   => $this->str_name_singular . ' Link',

			// Used in the block editor . Description for a navigation link block variation . default 'A link to a tag' / 'A link to a category'.
			'item_link_description'       => 'A link to a ' . $this->str_name_singular,

		);
		return $arr_labels;
	}
}

## WPezClasses: Taxonomy Labels (US)

__Create your WordPress taxonomy labels The ezWay.__

This particular class is for English (US). 


### Simple Example


_Recommended: Use the WPezClasses autoloader (link below)._


    use WPezSuite\WPezClasses\TaxonomyLabelsUS\ClassTaxonomyLabelsUS as Labels;
    
    $new_labels = new Labels();
    $new_labels->setNames('TEST Tax');
    $arr_labels = $new_labels->getLabels();


There are three public methods:

- setNameSingular( $str_singular ) - Sets the name singular

- setNamePlural( $str_plural ) - Sets the name plural

- setNames( $str_singular, $str_plural ) - Sets both via a single method. If the plural is not passed, the method will automatically append an 's' to the $str_singular that is passed. 


### FAQ

__1) Why?__

Using the WordPress taxonomy labels' defaults is a not-so-good UX, and crafting them by hand is time consuming. This solves both of those problems.   

__2) Why not use a i18n language file?__

Aside from adding friction, langauge translation - in the context of UX - can be more involved than phrase for phrase matching / substitution. This keeps it simple and adds some flexibility.

__3) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __4) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

- https://gitlab.com/wpezsuite/WPezClasses/WPezAutoload

- https://gitlab.com/wpezsuite/WPezClasses/TaxonomyArgs

- https://gitlab.com/wpezsuite/WPezClasses/TaxonomyRegistrer 

- https://codex.wordpress.org/Function_Reference/register_taxonomy



### TODO

n/a


### CHANGE LOG

-v.0.0.3 - Saturday 7 Jan 2023
    - UPDATE: Refactoring + comments of the three WPez repos for WP taxonomies.

- v0.0.1 - Saturday 20 April 2019
    - This has been in development for quite some time, but not independently repo'ed until now. Please pardon the delay. 